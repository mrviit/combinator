TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        combinator.cpp \
        main.cpp

HEADERS += \
    combinator.h
