#include <iostream>
#include <algorithm>
#include <numeric>

#include "combinator.h"

using namespace std;

vector<int> findTargetElements(vector<int> const& sample, int target) {
    size_t k = 2;
    Combinator c;
    c.setSampleSize(sample.size());

//    cout << "sample: " << endl;
//    for(int const& i : sample) {
//        cout << i << " ";
//    }
//    cout << endl;

    vector<vector<size_t> > indexCombines = c.allCombi(k);
//    cout << "all combination " << k << ": " << endl;
//    for(vector<size_t> const& indexCombi : indexCombines) {
//        for(size_t const& i : indexCombi) {
//            cout << sample.at(i) << " ";
//        }
//        cout << endl;
//    }
//    cout << endl;

//    vector<vector<int> > valueCombines;
//    std::transform(indexCombines.begin(), indexCombines.end(), std::back_inserter(valueCombines),
//                   [&sample](vector<size_t> const& indexes){
//        vector<int> values;
//        std::transform(indexes.begin(), indexes.end(), std::back_inserter(values),
//                       [&sample](size_t const& index){
//            return sample.at(index);
//        });
//        return values;
//    });

//    cout << "all combination " << k << ": " << endl;
//    for(vector<int> const& valueCombi : valueCombines) {
//        for(size_t const& value : valueCombi) {
//            cout << value << " ";
//        }
//        cout << endl;
//    }
//    cout << endl;

    vector<int> targets;
//    std::transform(valueCombines.begin(), valueCombines.end(), std::back_inserter(targets),
//                   [](vector<int> const& valueCombi){
//        return std::accumulate(valueCombi.begin(), valueCombi.end(), 0);
//    });
    std::transform(indexCombines.begin(), indexCombines.end(), std::back_inserter(targets),
                   [&sample](vector<size_t> const& indexCombi){
        int t = 0;
        for(size_t const& i: indexCombi) {
            t += sample.at(i);
        }
        return t;
    });

//    std::sort(targets.begin(), targets.end());

//    cout << "all available targets: " << endl;
//    for(int const& t : targets) {
//        cout << t << " ";
//    }
//    cout << endl;

    vector<int>::iterator it = std::find(targets.begin(), targets.end(), target);
    if(it == targets.end()) {
        vector<int> sortedTargets = targets;
        std::sort(sortedTargets.begin(), sortedTargets.end());
        vector<int>::iterator low, up;
        low = std::lower_bound (sortedTargets.begin(), sortedTargets.end(), target);
        up = std::upper_bound (sortedTargets.begin(), sortedTargets.end(), target);

        if(std::abs(target - *low) < std::abs(target - *up)) {
            target = *low;
        } else {
            target = *up;
        }
        it = std::find(targets.begin(), targets.end(), target);
    }

    size_t targetIndex = it - targets.begin();
    vector<size_t> determinedValuesIndex = indexCombines.at(targetIndex);
    vector<int> determindedValues;
    std::transform(determinedValuesIndex.begin(), determinedValuesIndex.end(), std::back_inserter(determindedValues),
                   [&sample](size_t const& index){
        return sample.at(index);
    });
    return determindedValues;
}
int main()
{
    vector<int> const sample = {1, 7, 9, 8, 11};
    int target = 16;

    vector<int> determindedValues = findTargetElements(sample, target);

    cout << "determined elements: ";
    for(int const& t : determindedValues) {
        cout << t << " ";
    }
    cout << endl;

    return 0;
}
