#ifndef COMBINATOR_H
#define COMBINATOR_H

#include <vector>
using namespace std;

class Combinator
{
public:
    Combinator();
    vector<size_t> nextCombi();
    vector<vector<size_t> > allCombi(size_t size);
    void setSampleSize(size_t size);
    void setCurrentCombiSize(size_t size);
    void reset();
private:
    vector<size_t> currentResult;
    size_t samepleSize;
    size_t currentCombiSize;
    size_t startPivot;
    size_t currentPivot;
};

#endif // COMBINATOR_H
