#include "combinator.h"

#include <algorithm>

Combinator::Combinator(): startPivot(0), currentPivot(0)
{
}

vector<size_t> Combinator::nextCombi()
{
    if(!currentResult.empty()) {
        currentPivot = currentResult.back();
        currentResult.pop_back();
    }
    while(currentResult.size() < currentCombiSize && startPivot <= samepleSize - currentCombiSize) {
        if(currentResult.empty()) {
            currentResult.push_back(startPivot);
        }

        if(!std::binary_search(currentResult.begin(), currentResult.end(), ++currentPivot)
                && currentPivot < samepleSize) {
            currentResult.push_back(currentPivot);
        } else if(currentPivot == samepleSize) {
            currentPivot = currentResult.back();
            currentResult.pop_back();
            if(currentResult.empty() && ++startPivot < samepleSize - currentCombiSize) {
                currentResult.push_back(startPivot);
            }
        }
    }

    return currentResult;
}

vector<vector<size_t> > Combinator::allCombi(size_t size)
{
    reset();
    setCurrentCombiSize(size);

    vector<vector<size_t> > result;
    vector<size_t> combi = nextCombi();
    while(!combi.empty()) {
        result.push_back(combi);
        combi = nextCombi();
    }

    return result;
}

void Combinator::setSampleSize(size_t size)
{
    samepleSize = size;
}

void Combinator::setCurrentCombiSize(size_t size)
{
    currentCombiSize = size;
}

void Combinator::reset()
{
    startPivot = 0;
    currentPivot = 0;
    currentResult.clear();
}
