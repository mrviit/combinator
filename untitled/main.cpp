#include <QCoreApplication>
#include <cstdio>
#include <QSslSocket>
#include <openssl/pem.h>
#include <openssl/x509.h>
#include <openssl/applink.c>
#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/rsa.h>
#include <openssl/evp.h>
#include <openssl/pem.h>
#include <iostream>

//apt install  libopenssl-dev
//apt-get install libssl1.0-dev
//https://stackoverflow.com/questions/41809556/generate-private-key-with-openssl
//https://stackoverflow.com/questions/5927164/how-to-generate-rsa-private-key-using-openssl
//https://stackoverflow.com/questions/30959142/generate-rsa-public-private-key-with-openssl
//https://stackoverflow.com/questions/12647220/reading-and-writing-rsa-keys-to-a-pem-file-in-c
//https://developers.yubico.com/PIV/Guides/Generating_keys_using_OpenSSL.html
// https://jumpnowtek.com/security/Code-signing-with-openssl.html

// **https://pagefault.blog/2019/04/22/how-to-sign-and-verify-using-openssl/

// **https://eclipsesource.com/blogs/2016/09/07/tutorial-code-signing-and-verification-with-openssl/
// https://stackoverflow.com/questions/14846366/openssl-private-key-read-errorpem-read-bio-rsaprivatekey/56026819

// https://medium.com/@bn121rajesh/understanding-rsa-public-key-70d900b1033c

//https://www.openssl.org/docs/man1.1.1/man3/EVP_PKEY_set1_RSA.html
//EVP_PKEY_get1_RSA
//EVP_PKEY_get0_RSA
//EVP_PKEY_assign_RSA
//EVP_PKEY_new_raw_private_key	EVP_PKEY_new_raw_public_key		EVP_PKEY_get_raw_public_key
//EVP_PKEY_assign_RSA
//EVP_PKEY_base_id() returns the type of pkey. For example an RSA key will return EVP_PKEY_RSA.
//int EVP_PKEY_id(const EVP_PKEY *pkey);
//int EVP_PKEY_base_id(const EVP_PKEY *pkey);
// int EVP_PKEY_type(int type);
//int EVP_PKEY_set_alias_type(EVP_PKEY *pkey, int type);
//ENGINE *EVP_PKEY_get0_engine(const EVP_PKEY *pkey);
//int EVP_PKEY_set1_engine(EVP_PKEY *pkey, ENGINE *engine);


/////
// openssl genrsa -out myprivate.pem 2048
// ssh-keygen -t rsa
// openssl rsa -in .\example_rsa -pubout -out .\example_rsa_pub.pem
// echo xin chao viet nam > codeToSign.txt
// openssl rsautl -sign -in .\codeToSign.txt -inkey .\example_rsa -out .\codeToSign.txt.sgn
// openssl rsautl -verify -inkey my-pub.pem -in in.txt.rsa -pubin

// the standard recommended  way of signing document in 4 steps:
// 1)Create digest of document to sign (sender)
// 2)Sign digest with private key (sender)
// 3)Create digest of document to verify (recipient)
// 4)Verify signature with public key (recipient)
// OpenSSL does this in two steps:
// openssl dgst -sha256 -sign my.key -out in.txt.sha256 in.txt 
// openssl dgst -sha256 -verify my-pub.pem -signature in.txt.sha256 in.txt  
// openssl rsautl -verify -in sig -inkey aa.pem

// openssl base64 -in data.sig -out data.sig.b64
// openssl enc -base64 -in filepath
// openssl base64 -in data.sig -out data.sig.b64
// openssl base64 -d -in data.sig.b64 -out data.sig		#decode

// openssl dgst -sign key.pem -keyform PEM -sha256 -out data.zip.sign -binary data.zip
// openssl dgst -verify key.pub -keyform PEM -sha256 -signature data.zip.sign -binary data.zip

///////////////////////////////////
void genKey() {

    //    SSL_library_init();
    //    SSL_load_error_strings();
    //    OpenSSL_add_all_algorithms();

    //    init_openssl();

    //    RSA_new();
    //        RSA *pRSA = RSA_generate_key(2048,RSA_3,NULL,NULL);
    //    create_rsa_key(pRSA);

    EVP_PKEY *pkey = EVP_PKEY_new();
    if (!pkey) {
        return;
    }

    RSA *rsa = RSA_generate_key(2048, 3, NULL, NULL);
    if (!EVP_PKEY_assign_RSA(pkey, rsa)) {
        return;
    }

    BIO *bio = BIO_new(BIO_s_mem());
    PEM_write_bio_RSAPrivateKey(bio, rsa, NULL, NULL, 0, NULL, NULL);

    int pem_pkey_size = BIO_pending(bio);
    char *pem_pkey = (char*) calloc((pem_pkey_size)+1, 1);
    BIO_read(bio, pem_pkey, pem_pkey_size);


    FILE *pkey_file = fopen("key.pem", "wb");
    if (!pkey_file) {
        std::cerr << "Unable to open \"key.pem\" for writing." << std::endl;
        return;
    }

    bool ret = PEM_write_PrivateKey(pkey_file, pkey, NULL, NULL, 0, NULL, NULL);
    fclose(pkey_file);

    if(!ret) {
        std::cerr << "Unable to write private key to disk." << std::endl;
        return;
    }
    std::cout << pem_pkey << std::endl;
}

RSA* createPrivateRSA(std::string key) {
    RSA *rsa = NULL;
    const char* c_string = key.c_str();
    BIO * keybio = BIO_new_mem_buf((void*)c_string, -1);
    if (keybio==NULL) {
        return 0;
    }
    rsa = PEM_read_bio_RSAPrivateKey(keybio, &rsa,NULL, NULL);
    return rsa;
}

bool RSASign( RSA* rsa,
              const unsigned char* Msg,
              size_t MsgLen,
              unsigned char** EncMsg,
              size_t* MsgLenEnc) {
    EVP_MD_CTX* m_RSASignCtx = EVP_MD_CTX_create();
    EVP_PKEY* priKey  = EVP_PKEY_new();
    EVP_PKEY_assign_RSA(priKey, rsa);
    if (EVP_DigestSignInit(m_RSASignCtx,NULL, EVP_sha256(), NULL,priKey)<=0) {
        return false;
    }
    if (EVP_DigestSignUpdate(m_RSASignCtx, Msg, MsgLen) <= 0) {
        return false;
    }
    if (EVP_DigestSignFinal(m_RSASignCtx, NULL, MsgLenEnc) <=0) {
        return false;
    }
    *EncMsg = (unsigned char*)malloc(*MsgLenEnc);
    if (EVP_DigestSignFinal(m_RSASignCtx, *EncMsg, MsgLenEnc) <= 0) {
        return false;
    }
    EVP_MD_CTX_free(m_RSASignCtx);
    return true;
}

void Base64Encode( const unsigned char* buffer,
                   size_t length,
                   char** base64Text) {
  BIO *bio, *b64;
  BUF_MEM *bufferPtr;

  b64 = BIO_new(BIO_f_base64());
  bio = BIO_new(BIO_s_mem());
  bio = BIO_push(b64, bio);

  BIO_write(bio, buffer, length);
  BIO_flush(bio);
  BIO_get_mem_ptr(bio, &bufferPtr);
  BIO_set_close(bio, BIO_NOCLOSE);
  BIO_free_all(bio);

  *base64Text=(*bufferPtr).data;
}

char* signMessage(std::string privateKey, std::string plainText) {
  RSA* privateRSA = createPrivateRSA(privateKey);
  unsigned char* encMessage;
  char* base64Text;
  size_t encMessageLength;
  RSASign(privateRSA, (unsigned char*) plainText.c_str(), plainText.length(), &encMessage, &encMessageLength);
  Base64Encode(encMessage, encMessageLength, &base64Text);
  free(encMessage);
  return base64Text;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    std::string plTxt = "okeabcd";
//    std::string sk = "LS0tLS1CRUdJTiBSU0EgUFJJVkFURSBLRVktLS0tLQpQcm9jLVR5cGU6IDQsRU5D"
//            "UllQVEVECkRFSy1JbmZvOiBBRVMtMTI4LUNCQyxGMjU0MDlDMTQyRUNCOTkzQkUx"
//            "RTQxNEFBODlGNjBDMgoKd0pNaDVuT3pqYjQvbUdud24yazRrbTAwb2JVcXdMQ05o"
//            "QXNHTXk5a0xvZTFTTy84Y3c5czFrYndHS05VMWtaNgpsZDRwRi9LOHBZNnljNFA5"
//            "MG9EVk5maEhDRjBYUzVLcGlYUmY5MDdZZ2FGak5BUm9ERHJwT1dlVW14UTVodHBY"
//            "CmtsbWl4alR1cWlGS3BSaWNsNzhTMlhHYkVHTXhpSXhzQ0JBWEh2akFvUytqdzE3"
//            "WmlKY1BETXZLTk9nUmNMeU8KaG5wVHVudGlvSTV4WEEreU9XbHJSM1Zyc2FoWUdW"
//            "UXd0VjFSTExrTzJxUzRKNFZja21BOVhaWjlwOTZIWXZEYwppbkMwNFdBU2RzRS9V"
//            "cVhMY00zU08yVllOUm55Z29XYWRTUjlUYThwdmNkNHRySktMQkw2WFJ0TGp6RC80"
//            "MnBZCm5HTDVMWFZ3MW9mQ21GWGJNa01uV080enlnSFlJZzBIUVVIMGRHQnRJdEFx"
//            "SE12aitQdXRva2hRaUdPNFcxeGUKcmQyd3F3aHA0ME1oRG1aRUVndGZyU1p4cHd3"
//            "UGNWYU40YUpOZXVpamF5MlcxSjluNmtwdEJBczZHUElKbDR5bwpKOFNqVWp4c010"
//            "MThDMjY5NGxtVUxIRWJDSUZjaDVNV1BqK0lJRC84VnJPWEl2QjhKNGR3aXI5UkF3"
//            "STRKdEF4ClFQTHZXZHBFUlRiN1RPb1lVbkEyMjkremZwV0ZTMlRYMS9wMEdDVFRG"
//            "bTRzZExxNjk3TnkwM1l6M2E2a3diZ3kKazJQYU1xOWk2djlCdzVzNndiNktEMEpM"
//            "NS84NGdQVlAvUTRtamZHSUVCTWF5RHBvV0NRWDdwZGVMNlJsQXp3Two4c0lFR1du"
//            "OGxraDM0V2NES0g4WlZ6dFU3TUdjRERiUlBnOEtLUTRKcU95b20yaHJKbldjTmRS"
//            "dlJRTUhjOE1uCnBZdDJVNjJ4MURPTDgzTHpDQVR1dUp5SmFDZ3lwVkNtWnVmSVZj"
//            "Z1orVWR3amV2YjZZOE1wT25ydThEWkpkNkgKTUM5VmZaSm5pc3owMVIwai9Jd3N5"
//            "VlZqOGNQSm1hQy9uRmM4VlZTTERWNEZiWThUQUV6YzFVSVhEYndiMTdEMAppSTVJ"
//            "R1BTR3RDTzY2Qkl1V3d6enZTRjB6N1ZDZVJ5eFUyZWFWNFBpZ0NRR2VXOHR3Mjlz"
//            "ZVIzTU42NUxVZ3FHCm9KNmxoTWFwdXhtMDlCUkhrbmVhejcrL0tzZjZEeVlLUXFK"
//            "VVZCclllWUpOMVBJbEZDaUtvZFdXajRMSmpiM0oKNFFRaTMvNXBVT255Q2NiMWI2"
//            "REZsVEVCWTlBMGJCQkdMeWVGVFpaU25NQ1NYQWNiN2ZwUjVvVGozdk02dktZcQpr"
//            "dDFud09mbkNwL3VSMnFUQmhkWldSKzhSWHh4UXRiNUR0bVArUEswYVRlcmFLWUYz"
//            "Sjh4TDhadmROdFNwZ1pzCjg2VjVHSEhaQ3d4ZEFEWThIenNmUHlMTTNVcURlWUdo"
//            "T0hSb0NpSGd0ZFZGY2loSGRKZDlVWmxGbTRsSUc2ek4KamdHL2xUUnNSVFdoMEFi"
//            "MUREaCtYaExqV3grOGNKaHJzSW5xdXJFZUlBVXorMXE4L0JVMVpaTTFqcGZkSndr"
//            "OApzSklKS3NGOHlSV3pvc3VFVTVjcllFcC9WVGg2ODBWWXJsZ1UrbU9OMWJ0NUEx"
//            "UkJLL3YvNTVQRmtadU1qWW52ClMyM01LQnBITDdRZVhZN1I2L1lJWnZqdlBLYkND"
//            "d2pEU2dVdGg0dy8rTExsT0JnVG44TjJ3UUJGMi8vQXJBQ0QKTXVzK2VYN1FaSjd0"
//            "SUxaMHhqOWNIdFlITzMwOG1OdysySCtXTlFlR29CTy9iNDBDRE1JU2gxalV4MVh4"
//            "ZVJwZwp2bEtqWWxvVUhndG1JRlpJSzlhQWZycjc4RWVCWlRvb2V5Ylo2QnhXdGIr"
//            "MjR4NjhUdi85WTF5Q1FCZkRNQmdzCks0by9aTm14TzU3T1ozYTkzVUZLdTFUVVdS"
//            "SWJjTHg4Q3E5OFZxRTgxUWx5d05qaFpLTXV6R21hQ2xuQlQ4ZzUKQzF1RExKcWMw"
//            "N3pBQURkOG9Objl2cGp6b3NtMzYvRDJDUVpqd3dxeWZhNHNMMktZcmM0NkNISVM0"
//            "NXFTYnkvUwotLS0tLUVORCBSU0EgUFJJVkFURSBLRVktLS0tLQo=";
    std::string sk = "MIIEpAIBAAKCAQEA383hghb5wLwmzs+lCQMWjkMclD/McQ4ppArmrsMndg9wk35Z"
            "JgGLciRU+Rzeainzdxr4HHtc2eVxJlX88+OMq8gYZkiyV/x0mN7duA9eMXFOLZ5L"
            "xG43IcyhsvJdy3wOKJHg1eRCTkSNE8UsYH9/OYjyotuaTBlxotzGhNMWcdbsnkLd"
            "x2QcMF4/m59lam3j0kRr0OLAI5hiBOX3b3ULqRLlln2unY3MFSTGq1ApaY55rur7"
            "2fTDiz49ygU3f0aX0Y8RF0rzdnW3b26TQvpiclXTryNfFtlCoYMX2bRSSO7zCpgo"
            "vnMK/mtzjqaw6Bd59WpwOHJjWe2M2ll6xfu3qwIDAQABAoIBAQCxS9pQQn9MjkF4"
            "KMsnHokp3kSvtPLyxTN7vx8MZ5HFV2npjnh1QVgE7n9aceJI3D8Bph3UAm6GN9ux"
            "cQFY/HUeSP/7V6/hQB/mUvf2rkYCiS5TuUlwrZeDcSbQKmzfnMFipl4lVBSrCGdy"
            "uAr1z0ssey8skSflDWsWiCvEYa0wNIYf+v1LU96AfWZot+Pz5aFWwCe73YtFCiYZ"
            "B8g22barlXLfxyVMpDBj9Cpq+8UlH288kqKZw/sC3NRiYebTGRNrGeLCPdcfOpuW"
            "V6xYLUpmykP6nljY17/Ees4kERl+d5oVugLbh8Ew/nDuH05niCbb+DqnF9vyiWbK"
            "s6NxFG1hAoGBAPQ2lIj+ZcYH+nMucvEHLMkJ0i0pbQr4Fwsm+2moOGfIQMe0ldAw"
            "To5oGGAsSJNzdRYs/Kz/Ejiv9L2guRT/TtXUSMPDNW/tGhavVYBn3iJis1b7cwED"
            "EAk1oeGaJXATLLKKlDbX0IkKDwb9Pqv6LXcBvmfrz7eOxUI6wKg2oSUpAoGBAOqb"
            "IkTbXrdBHRXvXcyfFlufL/jBGAuk9HqenVIXlJ5ojoMyD8OVkG/mo1eAMUA4IVrt"
            "hdFGKuTXKmuVdYxIDWvgRleajYYnjQVsgQC2CsCyXolC6KusVXRpuiB9MpIESX4e"
            "irZMjXCBZ6q0LXKvnW73bIejFKM91R8GOcFE1FyzAoGAQdzyhFvwAM+oCSZu03h2"
            "6KVgbNOuV/X+6QATcDRxOfQ1HGckvGg614dVg6KN1E6gNmHdp56HQIib/gR5xiLx"
            "FRRSp5FfORd+IL4a1i2tKCLscToRkyVLTuCLDzgufqJ9PI3JWV0tHbh7wwvLukAX"
            "wO/UdVyQwdWcd7c2vGc3YLkCgYEA53aFWfAhQ6YPXzvZjG+M7HpjJwbDhYGK34us"
            "ZXj0XVGFAXWbzfi7F1wy3bqRXO3EIuTjOPUEjuCmbpx37B6Od1c/r2DBrpFI9gf2"
            "QtBYTYPt93i2++E8Mm25gCJliWCHbdIOQwvGJDDYKbg4esU/C2M7hZrlWqU/pqtK"
            "wh75cjcCgYAt5dTnb21i1uEnVS+fJ+z8XN5CI3RRMwS8iBGeQuIS5WGrngek4UX7"
            "FcmZdAhJGZJjvSm7y7MQp8miM1zbFBaq4IUtem0IdxhDDJytlWNlgIlGIn1TWJ7c"
            "tkHfOvNhM+Fbuo5Z88veuH33bzGEioNtYxIGDyht/2XKZc0l9wmbrA==";
    qDebug() << sk.data();
    qDebug() << plTxt.data();
    char* rs = signMessage(sk, plTxt);

    qDebug() << *rs;
    return a.exec();
}
